<?php

namespace app\domain\validator;

use app\domain\model\Model;

abstract class Validator
{
    /**
     * @var array
     */
    protected $errors = [];

    /**
     * @param Model $model
     * @return bool
     */
    abstract public function validate(Model $model);
    
    public function getErrors()
    {
        return $this->errors;
    }

    public function hasErrors()
    {
        return !empty($this->errors);
    }
}