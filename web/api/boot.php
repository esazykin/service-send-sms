<?php

require projectPath() . '/vendor/autoload.php';

function projectPath() {
	static $path;
	if (is_null($path)) {
		$path = realpath(__DIR__ . '/../..');
	}
	return $path;
}

/**
 * @param $key string
 * @return string|array
 */
function lang($key) {
	static $lang;
	if (is_null($lang)) {
		$lang = require projectPath() . '/app/lang/en.php';
	}
	return isset($lang[$key]) ? $lang[$key] : '';
}

{
	$app = new Silex\Application();
	foreach (require(projectPath() . '/app/config/common.php') as $k => $v) {
		$app[$k] = $v;
	}
}
return $app;