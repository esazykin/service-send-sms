<?php

if (file_exists(__DIR__ . '/db.local.php')) {
	return include __DIR__ . '/db.local.php';
}

return [
	'driver' => 'pdo_mysql',
	'user' => 'root',
	'password' => 'root',
	'dbname' => 'service_send_sms',
];