<?php

namespace app\data;

use app\domain\model\AuthCode;
use DateTime;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\Query\Expr\OrderBy;

class AuthCodeRepository extends EntityRepository
{
	const TIME_INTERVAL = '-40 seconds';

	/**
	 * @param string $phone
	 * @return int
	 */
	public function alreadyExists($phone)
	{
		$qb = $this->createQueryBuilder('ac')
			->select('count(ac.id)')
			->where('ac.phone = :phone')
			->andWhere('(ac.status = :status OR ac.created >= :now_sub_interval)')

			->setParameter('phone', $phone)
			->setParameter('status', AuthCode::STATUS_TAKEN)
			->setParameter('now_sub_interval', new DateTime(self::TIME_INTERVAL));

		try {
			return (int)$qb->getQuery()->getSingleScalarResult();
		} catch (\Exception $e) {
			return 0;
		}
	}

	/**
	 * get unprocessed codes
	 * @return AuthCode[]
	 */
	public function getTaken()
	{
		$qb = $this->createQueryBuilder('ac')
			->select('ac, p')
			->where('ac.status = :status')
			->join('ac.phone', 'p')
			->setMaxResults(10)
			->setParameter('status', AuthCode::STATUS_TAKEN);

		return $qb->getQuery()->getResult();
	}

	public function getForStatistic(DateTime $lastDate)
	{
		$qb = $this->createQueryBuilder('ac')
			->where('ac.created >= :created')
			->andWhere('ac.status IN(:status)')
			->orderBy(new OrderBy('ac.id', 'ASC'))
			->setMaxResults(1)
			->setParameter('created', $lastDate)
			->setParameter('status', [AuthCode::STATUS_PROCESSED, AuthCode::STATUS_CONFIRMED]);

		try {
			return $qb->getQuery()->getSingleResult();
		} catch (NoResultException $e) {
			return null;
		} catch (NonUniqueResultException $e) {
			return null;
		}

	}

	public function getLast()
	{
		$statement = $this->getEntityManager()->getConnection()->prepare(
			'SELECT id, status, DATE_FORMAT(created, "%Y-%m-%d") AS date
			FROM auth_code
			WHERE created < CURRENT_DATE
				AND id > :lastId
				AND status IN(:st_c, :st_p)
			ORDER BY id ASC
			LIMIT 500'
		);
		$statement->bindValue('st_c', AuthCode::STATUS_CONFIRMED);
		$statement->bindValue('st_p', AuthCode::STATUS_PROCESSED);
		return $statement;
	}
}
