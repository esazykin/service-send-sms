<?php

namespace app\domain\model;

use DateTime;

/**
 * @Entity
 * @Table(
 * 		name="statistic_verification",
 *		uniqueConstraints={
 * 			@UniqueConstraint(name="idx1", columns={"date"})
 * 		}
 * )
 */
class StatisticVerification extends Model
{
	/**
	 * @Column(type="date")
	 * @var DateTime
	 */
	protected $date;

	/**
	 * @Column(type="integer", options={"unsigned"=true})
	 * @var int
	 */
	protected $confirmed;

	/**
	 * @Column(type="integer", options={"unsigned"=true}, name="not_confirmed")
	 * @var int
	 */
	protected $notConfirmed;

	public function getDate()
	{
		return $this->date;
	}

	public function setDate($date)
	{
		$this->date = $date;
	}

	public function getConfirmed()
	{
		return $this->confirmed;
	}

	public function setConfirmed($confirmed)
	{
		$this->confirmed = $confirmed;
	}

	public function getNotConfirmed()
	{
		return $this->notConfirmed;
	}

	public function setNotConfirmed($notConfirmed)
	{
		$this->notConfirmed = $notConfirmed;
	}
}
