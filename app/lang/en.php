<?php

return [
    'error.missing' => 'Field not found',
    'error.object_not_found' => '%s not found',
    'error.type_mismatch' => 'Type mismatch',
    'error.not_unique' => 'The field already used',
    'error.empty' => 'The field is empty',
    'error.phone.not_valid' => 'Phone number does not seem to be a valid phone number',
    'error.phone.default' => 'Unexpected Phone Number Format or Country Code',
    'error.phone.check_last' => 'Very often requests',
    'error.phone.already_verify' => 'The code already verified',
    'error.phone.not_processed' => 'Not yet processed',
    'error.phone.time_out' => 'Timeout of verification',
    'rpcResponseError' => [
        -32700 => 'Parse error',
        -32600 => 'Invalid Request',
        -32601 => 'Method not found',
        -32602 => 'Invalid params',
        -32603 => 'Internal error',
        -32000 => 'Server error',
    ],
];