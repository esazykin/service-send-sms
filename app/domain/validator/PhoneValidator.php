<?php

namespace app\domain\validator;

use app\domain\model\Model;
use Exception;
use libphonenumber\NumberParseException;
use libphonenumber\PhoneNumber;
use libphonenumber\PhoneNumberUtil;

class PhoneValidator extends Validator
{
    /**
     * @var PhoneNumber
     */
    protected $phoneNumber;

    public function validate(Model $model)
    {
        if (!method_exists($model, 'getPhone')) {
            $this->errors['phone'] = ['missing' => lang('error.missing')];
            return false;
        }
        if (empty($model->getPhone())) {
            $this->errors['phone'] = ['empty' => lang('error.empty')];
            return false;
        }

        return $this->validateValue($model->getPhone());
    }

    public function validateValue($phone)
    {
        $phoneUtil = PhoneNumberUtil::getInstance();
        try {
            $this->phoneNumber = $phoneUtil->parse($phone, $countryCode = '');
            if ($phoneUtil->isValidNumber($this->phoneNumber)) {
                return true;
            } else {
                $this->errors['phone'] = ['phone.not_valid' => lang('error.phone.not_valid')];
                return false;
            }
        } catch (NumberParseException $e) {
            $this->errors['phone'] = ['phone.parse' => (string)$e];
        } catch (Exception $e) {
            $this->errors['phone'] = ['phone.default' => lang('error.phone.default')];
        }
        return false;
    }

    public function getPurePhoneNumber()
    {
        return $this->phoneNumber->getCountryCode() . $this->phoneNumber->getNationalNumber();
    }
}