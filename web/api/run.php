<?php
/**
 * @var $app Application
 */

use app\rpc\response\body\RpcResponseError;
use app\rpc\response\RpcResponse;
use app\system\ErrorHandler;
use Doctrine\Common\Cache\FilesystemCache;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;
use Silex\Application;
use app\rpc\Presenter;
use app\rpc\request\RpcRequest;
use app\rpc\validator\RpcRequestValidator;
use Symfony\Component\HttpFoundation\Request;

{
	$config = Setup::createAnnotationMetadataConfiguration([projectPath() . "/app/domain/model"], $app['debug']);
	$config->setMetadataCacheImpl(new FilesystemCache(projectPath() . "/runtime/cache/metadata"));
	$app['doctrineManager'] = EntityManager::create($app['db'], $config);

	$whoops = new \Whoops\Run;
	$whoops->pushHandler(new ErrorHandler($app['debug']));
	$whoops->register();
}
$app
	->match('/api', function (Application $app, RpcRequest $rpcRequest) {
		$response = (new Presenter(
			new RpcRequestValidator($app['methodNamespace']),
			$app['methodNamespace'],
			$app['doctrineManager']
		))->process($rpcRequest);
		return $app->json($response);
	})
	->method('GET|POST')
	->convert('rpcRequest', function ($rpcRequest, Request $request) {
		if (0 === stripos($request->headers->get('Content-Type'), 'application/json')) {
			$data = json_decode($request->getContent(), true);
			if (is_array($data)) {
				$request->request->replace(array_change_key_case($data));
				return new RpcRequest(
					$request->get("id", ''),
					$request->get("jsonrpc", '0'),
					$request->get("method", ''),
					$request->get("params", [])
				);
			}
		}
		return null;
	});
$app->error(function (\Exception $e, $code) use ($app) {
	$body = new RpcResponseError();
	$body->setCode(RpcResponseError::CODE_PARSE_ERROR);
	$r = new RpcResponse(null, null, $body);
	return $app->json($r);
});
$app->run();