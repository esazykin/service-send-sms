<?php

namespace console\command;

use app\data\AuthCodeRepository;
use app\domain\model\AuthCode;
use app\domain\model\StatisticVerification;
use Doctrine\ORM\EntityManager;
use Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class StatisticVerificationCommand
 *
 * add this command in cron
 * 59 23 * * * <path_to_project>/console/index statistic-verification
 *
 * @package console\command
 */
class StatisticVerificationCommand extends Command
{
	const MODIFY_DATE = "+1 day";

	/**
	 * @var EntityManager
	 */
	protected $doctrine;

	public function setEntityManager(EntityManager $doctrine)
	{
		$this->doctrine = $doctrine;
		return $this;
	}

	protected function configure()
	{
		$this
			->setName('statistic-verification')
			->setDescription('Aggregating of verifications');
	}

	protected function execute(InputInterface $input, OutputInterface $output)
	{
		/** @var AuthCodeRepository $authCodeRepository */
		$authCodeRepository = $this->doctrine->getRepository(AuthCode::class);
		$lastId = $this->retrieveAuthCodeLastId();
		if ($lastId < 0) {
			return;
		}

		$inserted = [];
		$statement = $authCodeRepository->getLast();
		do {
			$statement->bindValue('lastId', $lastId);
			$statement->execute();
			while ($row = $statement->fetch()) {
				if (!isset($inserted[$row['date']])) {
					$inserted[$row['date']] = ['date' => $row['date'], 'confirmed' => 0, 'not_confirmed' => 0];
				}
				if ($row['status'] == AuthCode::STATUS_PROCESSED) {
					$inserted[$row['date']]['not_confirmed']++;
				} elseif ($row['status'] == AuthCode::STATUS_CONFIRMED) {
					$inserted[$row['date']]['confirmed']++;
				}
				$lastId = $row['id'];
			}
		} while ($statement->rowCount() > 0);
		if (empty($inserted)) {
			return;
		}
		$inserted = implode(',', array_map(function($v) {
			return "('$v[date]', $v[confirmed], $v[not_confirmed])";
		}, $inserted));

		try {
			$count = $this->doctrine->getConnection()->executeUpdate(
				"INSERT IGNORE INTO statistic_verification(date, confirmed, not_confirmed) VALUES $inserted"
			);
			$output->writeln("{inserted: $count, lastId: $lastId}");
		} catch (Exception $e) {
			$output->writeln($e->getMessage());
		}
	}

	protected function retrieveAuthCodeLastId()
	{
		$authCodeRepository = $this->doctrine->getRepository(AuthCode::class);
		$svRepository = $this->doctrine->getRepository(StatisticVerification::class);

		$lastRecord = $svRepository->findOneBy([], ['date' => 'DESC']);
		$lastId = 0;
		if ($lastRecord) {
			$authCode = $authCodeRepository->getForStatistic($lastRecord->getDate()->modify(self::MODIFY_DATE));
			if ($authCode) {
				$lastId = $authCode->getId() - 1;
			} else {
				return -1;
			}
		}
		return $lastId;
	}

}
