<?php

require projectPath() . '/vendor/autoload.php';

function projectPath() {
	static $path;
	if (is_null($path)) {
		$path = realpath(__DIR__ . '/..');
	}
	return $path;
}
