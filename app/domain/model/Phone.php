<?php

namespace app\domain\model;
use libphonenumber\NumberParseException;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberUtil;

/**
 * @Entity
 * @Table(name="phone")
 */
class Phone extends Model
{
    /**
     * @Column(type="bigint", unique=true, options={"unsigned"=true})
     * @var int
     */
    protected $phone;

    public function getPhone()
    {
        return $this->phone;
    }

    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    public function getFormattedPhone()
    {
        $phoneUtil = PhoneNumberUtil::getInstance();
        try {
            $p = $phoneUtil->parse('+' . $this->getPhone(), $countryCode = '');
        } catch (NumberParseException $e) {
            return '';
        }
        return $phoneUtil->format($p, PhoneNumberFormat::INTERNATIONAL);
    }
}
