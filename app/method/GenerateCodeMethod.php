<?php

namespace app\method;

use app\domain\model\AuthCode;
use app\domain\model\Phone;
use app\domain\validator\PhoneValidator;
use app\rpc\response\body\RpcResponseBody;
use app\rpc\response\body\RpcResponseResult;
use DateTime;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMInvalidArgumentException;

class GenerateCodeMethod extends Method
{
    /**
     * @param array $params
     * @return RpcResponseBody
     */
    public function run(array $params)
    {
        if (empty($params['phone'])) {
            return $this->createError(['phone' => ['missing' => lang('error.missing')]]);
        }

        $validator = new PhoneValidator();
        if (!$validator->validateValue($params['phone'])) {
            return $this->createError($validator->getErrors());
        }
        $phoneRepository = $this->doctrine->getRepository(Phone::class);
        $phone = $phoneRepository->findOneBy(['phone' => $validator->getPurePhoneNumber()]);
        if ($phone) {
            $authCodeRepository = $this->doctrine->getRepository(AuthCode::class);
            if ($authCodeRepository->alreadyExists($phone)) {
                return $this->createError(['phone' => ['phone.check_last' => lang('error.phone.check_last')]]);
            }
        } else {
            $phone = new Phone();
            $phone->setPhone($validator->getPurePhoneNumber());
            try {
                $this->doctrine->persist($phone);
            } catch (ORMInvalidArgumentException $e) {
                return $this->createError(['phone' => ['unknown' => $e->getMessage()]]);
            }
        }

        $authCode = new AuthCode();
        $authCode->setCreated(new DateTime());
        $authCode->setPhone($phone);
        $authCode->generateCode();
        try {
            $this->doctrine->persist($authCode);
        } catch (ORMInvalidArgumentException $e) {
            return $this->createError(['code' => ['unknown' => $e->getMessage()]]);
        }

        try {
            $this->doctrine->flush();
        } catch (UniqueConstraintViolationException $e) {
            return $this->createError(['phone' => ['not_unique' => lang('error.not_unique')]]);
        } catch (OptimisticLockException $e) {
            return $this->createError(['phone' => ['unknown' => (string)$e]]);
        }

        return new RpcResponseResult(['token' => $authCode->getId()]);
    }
}
