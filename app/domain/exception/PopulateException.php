<?php

namespace app\domain\exception;


use Exception;

class PopulateException extends \Exception
{
    /**
     * @var string
     */
    private $field;

    /**
     * @param string $field
     */
    public function __construct($field)
    {
        parent::__construct(lang('error.missing'));
        $this->field = $field;
    }

    public function getField()
    {
        return $this->field;
    }
}
