<?php

namespace app\domain\model;
use DateTime;

/**
 * @Entity(repositoryClass="app\data\AuthCodeRepository")
 * @Table(
 *      name="auth_code",
 * 		indexes={
 * 			@Index(name="idx_created", columns={"created"})
 * 		}
 * )
 */
class AuthCode extends Model
{
    const STATUS_TAKEN = 0;
    const STATUS_PROCESSED = 10;
    const STATUS_CONFIRMED = 20;

    /**
     * @Column(type="smallint", options={"unsigned"=true})
     * @var int
     */
    protected $code;

    /**
     * @ManyToOne(targetEntity="Phone")
     * @JoinColumn(name="phone_id", referencedColumnName="id", onDelete="CASCADE")
     * @var Phone
     */
    protected $phone;

    /**
     * @Column(type="datetime")
     * @var DateTime
     */
    protected $created;

    /**
     * @Column(type="smallint")
     * @var int
     */
    protected $status = self::STATUS_TAKEN;

    public function getCreated()
    {
        return $this->created;
    }

    public function setCreated($created)
    {
        $this->created = $created;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($status)
    {
        $this->status = $status;
    }

    public function getPhone()
    {
        return $this->phone;
    }

    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function generateCode()
    {
        $this->code = mt_rand(100, 999);
    }
}
