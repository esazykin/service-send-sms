<?php

namespace app\method;


use app\domain\model\AuthCode;
use app\rpc\response\body\RpcResponseResult;
use DateTime;
use Exception;

class VerifyCodeMethod extends Method
{
	const STATUS_SUCCESS = 1;

    public function run(array $params)
    {
        if (empty($params['code'])) {
			return $this->createError(['code' => ['missing' => lang('error.missing')]]);
        }
        if (empty($params['token'])) {
			return $this->createError(['token' => ['missing' => lang('error.missing')]]);
        }
        $authCodeRepository = $this->doctrine->getRepository(AuthCode::class);
		/** @var AuthCode $authCode */
		// may be cached
		$authCode = $authCodeRepository->find($params['token']);
		if (!$authCode || $authCode->getCode() != $params['code']) {
			return $this->createError(['code' => ['object_not_found' => sprintf(lang('error.object_not_found'), 'Code')]]);
		}
		if ($authCode->getStatus() == AuthCode::STATUS_CONFIRMED) {
			return $this->createError(['code' => ['phone.already_verify' => lang('error.phone.already_verify')]]);
		}
		if ($authCode->getStatus() != AuthCode::STATUS_PROCESSED) {
			return $this->createError(['code' => ['phone.not_processed' => lang('error.phone.not_processed')]]);
		}

		$dateInterval = (new DateTime())->diff($authCode->getCreated());
		if (!$dateInterval || $dateInterval->d > 0) {
			return $this->createError(['code' => ['phone.time_out' => lang('error.phone.time_out')]]);
		}

		$authCode->setStatus(AuthCode::STATUS_CONFIRMED);
		try {
			$this->doctrine->persist($authCode);
			$this->doctrine->flush();
		} catch (Exception $e) {
			return $this->createError(['code' => $e->getMessage()]);
		}

		return new RpcResponseResult(['status' => self::STATUS_SUCCESS,]);
    }
}