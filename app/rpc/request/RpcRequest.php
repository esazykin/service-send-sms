<?php

namespace app\rpc\request;

class RpcRequest implements RpcRequestBase
{
	/**
	 * @var string
	 */
	private $id;

	/**
	 * @var string
	 */
	private $version;

	/**
	 * @var string
	 */
	private $method;

	/**
	 * @var array
	 */
	private $params;

	public function __construct($id, $version, $method, array $params)
	{
		$this->id = $id;
		$this->version = $version;
		$this->method = $method;
		$this->params = $params;
	}

	/**
	 * @return string
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @return string
	 */
	public function getVersion()
	{
		return $this->version;
	}

	/**
	 * @return string
	 */
	public function getMethod()
	{
		return $this->method;
	}

	/**
	 * @return array
	 */
	public function getParams()
	{
		return $this->params;
	}
}