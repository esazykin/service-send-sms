<?php

namespace console\command;

use app\data\AuthCodeRepository;
use app\domain\model\AuthCode;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMInvalidArgumentException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

declare(ticks = 1);

class SendCodeCommand extends Command
{
	const JSON_RESPONSE = 3;
	const SLEEP = 10;

	/**
	 * @var EntityManager
	 */
	protected $doctrine;

	protected $smsConfig;

	public function setEntityManager(EntityManager $doctrine)
	{
		$this->doctrine = $doctrine;
		return $this;
	}

	public function setSmsConfig(array $conf)
	{
		$this->smsConfig = $conf;
		return $this;
	}

	protected function configure()
	{
		$this
			->setName('send-code')
			->setDescription('Send code ');
	}

	protected function execute(InputInterface $input, OutputInterface $output)
	{
		if (file_exists($this->getPidFilePath())) {
			$output->writeln("This daemon already running");
			return;
		}
		$this->lock();

		/** @var AuthCodeRepository $authCodeRepository */
		$authCodeRepository = $this->doctrine->getRepository(AuthCode::class);
		while (true) {
			$authCodes = $authCodeRepository->getTaken();
			if (empty($authCodes)) {
				sleep(self::SLEEP);
				continue;
			}
			$post = http_build_query([
				'fmt' => self::JSON_RESPONSE,
				'login' => $this->smsConfig['login'],
				'psw' => $this->smsConfig['pass'],
				'list' => implode("\n", array_map(function (AuthCode $authCode) {
					return sprintf("+%s:%s", $authCode->getPhone()->getPhone(), $authCode->getCode());
				}, $authCodes))
			]);

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $this->smsConfig['url']);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$r = curl_exec($ch);
			curl_close($ch);

			$json = json_decode($r, true);

			foreach ($authCodes as $authCode) {
				$authCode->setStatus(AuthCode::STATUS_PROCESSED);
				try {
					$this->doctrine->persist($authCode);
				} catch (ORMInvalidArgumentException $e) {
					$output->writeln('#' . $authCode->getId() . ' ' . $e->getMessage());
				}
			}
			try {
				$this->doctrine->flush();
			} catch (OptimisticLockException $e) {
				$output->writeln($e->getMessage());
			}

			if (isset($json['error_code'])) {
				$output->writeln($r);
				// do something ...
				sleep(self::SLEEP);
			}
		}
	}

	protected function getPidFilePath()
	{
		return projectPath() . "/runtime/send-code-command.pid";
	}

	protected function lock()
	{
		pcntl_signal(SIGTERM, [$this, "unlock"]);
		pcntl_signal(SIGHUP, [$this, "unlock"]);
		pcntl_signal(SIGUSR1, [$this, "unlock"]);
		pcntl_signal(SIGINT, [$this, "unlock"]);

		$pid = posix_getpid();
		file_put_contents($this->getPidFilePath(), $pid);
		echo "This daemon is running with pid $pid" . PHP_EOL;
	}

	protected function unlock($signo)
	{
		echo "Bye" . PHP_EOL;
		unlink($this->getPidFilePath());
		exit(0);
	}
}
